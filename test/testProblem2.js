//importing promisesSettle function
const promisesSettle = require("../problem2");

let promise1 = Promise.resolve(5);
let promise2 = Promise.reject("failed");
let promise3 = new Promise((resolve, reject) => {
  resolve("success");
});

//executing the promisesSettle function
promisesSettle([promise1, promise2, promise3]);
