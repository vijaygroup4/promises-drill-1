//declaring racePromise1 function
function racePromise1() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("success");
    }, 1000);
  });
}
//declaring racePromise2 function
function racePromise2() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      reject("failure");
    }, 3000);
  });
}

//declaring racePromises function
async function racePromises() {
  let promise1 = racePromise1();
  let promise2 = racePromise2();
  try {
    //Promise.race returns first promise execution result
    let output = await Promise.race([promise1, promise2]);
    console.log(output);
  } catch (error) {
    console.log(error);
  }
}

//exporting the above function
module.exports = racePromises;
