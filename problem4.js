//declaring parallelLimit function
async function parallelLimit(arrayOfPromises, limit) {
  //finding no of promises to run using Promise.all
  let noOfPromisesToRun = Math.min(limit, arrayOfPromises.length);
  const initialPromises = arrayOfPromises.slice(0, noOfPromisesToRun);

  try {
    //Promise.all method returns error if any promise returns error,otherwise all resolved results
    let results = await Promise.allSettled(initialPromises);

    let remainingPromises = arrayOfPromises.slice(initialPromises.length);
    //iterating through remaining promises and storing result in results array
    for (let promise of remainingPromises) {
      let run = async () => {
        try {
          let currentOutput = await promise;
          results.push(currentOutput);
        } catch (error) {
          //if error occurs,the below string is stored in results
          results.push("Promise rejected");
        }
      };
      await run();
    }

    return results;
  } catch (error) {
    console.log(error);
  }
}

//declaring runAllPromises function
async function runAllPromises() {
  //creating some promises
  let promise1 = Promise.resolve(5);
  let promise2 = Promise.resolve("good");
  let promise3 = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("success");
    }, 2000);
  });
  let promise4 = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("ok");
    }, 1000);
  });

  let limit = 2;
  const arrayOfPromises = [promise1, promise2, promise3, promise4];
  try {
    let output = await parallelLimit(arrayOfPromises, limit);
    console.log(output);
  } catch (error) {
    console.log(error);
  }
}

//exporting the above function
module.exports = runAllPromises;
