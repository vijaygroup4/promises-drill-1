//declaring dynamicChain function
async function dynamicChain(arrayOfFunctions) {
  //creating the chain by using reduce method
  let finalPromise = arrayOfFunctions.reduce(
    (accumulatorChain, currentFunction, index, array) => {
      return accumulatorChain.then(currentFunction);
    },
    Promise.resolve()
  );

  return finalPromise;
}

//declaring createChain function
async function createChain() {
  //creating some functions
  let function1 = () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve("function one");
      }, 1000);
    });
  };
  let function2 = () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve("function two");
      }, 1000);
    });
  };
  let function3 = () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve("function three");
      }, 1000);
    });
  };

  const arrayOfFunctions = [function1, function2, function3];

  try {
    let output = await dynamicChain(arrayOfFunctions);
    console.log(output);
  } catch (error) {
    console.log(error);
  }
}

//exporting the above function
module.exports = createChain;
