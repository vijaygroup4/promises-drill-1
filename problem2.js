//declaring composePromises function
async function composePromises(arrayOfPromises) {
  try {
    //Promise.allSettled method returns all promises results
    let outputPromise = await Promise.allSettled(arrayOfPromises);
    return outputPromise;
  } catch (error) {
    console.log(error);
  }
}

//declaring promisesSettle function
async function promisesSettle(arrayOfPromises) {
  try {
    const output = await composePromises(arrayOfPromises);
    console.log(output);
  } catch (error) {
    console.log(error);
  }
}

//exporting the above function
module.exports = promisesSettle;
